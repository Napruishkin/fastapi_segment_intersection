from fastapi import FastAPI, HTTPException
from app.schemas import Segment, RectangleResponse
from app.database import get_intersecting_rectangles_from_db

app = FastAPI()


@app.post("/intersecting_rectangles/")
async def get_intersecting_rectangles(segment: Segment):
    rectangles = get_intersecting_rectangles_from_db(segment)
    if not rectangles:
        raise HTTPException(status_code=404, detail="No intersecting rectangles found")
    return [RectangleResponse(**r.__dict__) for r in rectangles]
