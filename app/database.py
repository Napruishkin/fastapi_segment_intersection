from sqlalchemy.orm import sessionmaker

from app.models import Rectangle, engine

Session = sessionmaker(bind=engine)


def get_intersecting_rectangles_from_db(segment):
    session = Session()
    try:
        rectangles = session.query(Rectangle).filter(
            (Rectangle.x1 <= segment.x2) & (Rectangle.x2 >= segment.x1) &
            (Rectangle.y1 <= segment.y2) & (Rectangle.y2 >= segment.y1)
        ).all()
        return rectangles
    finally:
        session.close()
