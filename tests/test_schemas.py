from ..app.schemas import Segment, RectangleResponse


def test_segment_init():
    segment = Segment(x1=1, y1=1, x2=3, y2=3)
    assert segment.x1 == 1
    assert segment.y1 == 1
    assert segment.x2 == 3
    assert segment.y2 == 3


def test_rectangle_response_init():
    rectangle = RectangleResponse(id=1, x1=1, y1=1, x2=3, y2=3)
    assert rectangle.id == 1
    assert rectangle.x1 == 1
    assert rectangle.y1 == 1
    assert rectangle.x2 == 3
    assert rectangle.y2 == 3
