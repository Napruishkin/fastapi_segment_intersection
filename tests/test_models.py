from ..app.models import Rectangle


def test_rectangle_init():
    rectangle = Rectangle(x1=1, y1=1, x2=3, y2=3)
    assert rectangle.x1 == 1
    assert rectangle.y1 == 1
    assert rectangle.x2 == 3
    assert rectangle.y2 == 3
