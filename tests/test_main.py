from fastapi.testclient import TestClient
from ..main import app

client = TestClient(app)


def test_get_intersecting_rectangles():
    segment = {"x1": 1, "y1": 1, "x2": 3, "y2": 3}
    response = client.post("/intersecting_rectangles/", json=segment)
    assert response.status_code == 200
    assert len(response.json()) > 0


def test_get_intersecting_rectangles_no_results():
    segment = {"x1": 10, "y1": 10, "x2": 15, "y2": 15}
    response = client.post("/intersecting_rectangles/", json=segment)
    assert response.status_code == 404
