# FastAPI Segment Intersection API

This API accepts two pairs of doubles, representing a segment, and returns a list of rectangles that intersect the input segment by any of the edges.

### To run the API, execute the following command:
```
uvicorn main:app --reload
```

### To run the tests, execute the following command:
```
pytest
```
